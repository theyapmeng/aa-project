import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class DataService {


  public entryCount: number = 0;
  public entryList: { id: number, stockId: string, stockName: string, takenQuantity: number, quantifierName: string }[] = [
    { id: ++this.entryCount, stockId: "STK1200000", stockName: "ZIC SUPERVIS AW 68", takenQuantity: 20, quantifierName: "Bottle" },
    { id: ++this.entryCount, stockId: "STK1200001", stockName: "CROWN GREASE EP-2", takenQuantity: 5, quantifierName: "Bottle" },
    { id: ++this.entryCount, stockId: "STK1200002", stockName: "	ZIC SU[ER A", takenQuantity: 5, quantifierName: "Can" }
  ];

  private messageSource = new BehaviorSubject<any>(this.entryList);
  currentMessage = this.messageSource.asObservable();
  constructor() { }


  updateList(updatedList: any) {
   // this.entryList = updatedList;
    this.messageSource.next(updatedList);
  }

}