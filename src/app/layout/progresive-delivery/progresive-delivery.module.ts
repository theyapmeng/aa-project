import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProgresiveDeliveryRoutingModule } from './progresive-delivery-routing.module';
import { ProgresiveDeliveryComponent } from './progresive-delivery.component';
import { FormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    ProgresiveDeliveryRoutingModule,
    FormsModule
  ],
  declarations: [ProgresiveDeliveryComponent]
})
export class ProgresiveDeliveryModule { }
