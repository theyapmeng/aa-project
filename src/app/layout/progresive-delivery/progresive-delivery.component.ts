import { Component, OnInit ,Injectable,Input, Output} from '@angular/core';
import { DataService } from "../../data.service";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-progresive-delivery',
  templateUrl: './progresive-delivery.component.html',
  styleUrls: ['./progresive-delivery.component.scss']
})

@Injectable()
export class ProgresiveDeliveryComponent implements OnInit {

  
  public selectedCustomer: string;
  public selectedStock: { id: string, desc: string };
  public selectedQuantity: number;
  public entryCount: number = 0;
  public selectedQuantifier: string = "Bottle";

  public customerList: Array<string> = [];
  //public stockList: Array<Object> = [];

  public stockList: { id: string, desc: string }[] = [
    { "id": "STK1200000", "desc": "ZIC SUPERVIS AW 68" },
    { "id": "STK1200001", "desc": "CROWN GREASE EP-2" }
  ];

  public entryList: any;
  constructor(private data: DataService) {
  this.data.currentMessage.subscribe(message => this.entryList = message)
 // this.entryList = data.entryList;
   }

  ngOnInit() {
    this.customerList.push("Customer 1");
    this.customerList.push("Customer 2");
    this.customerList.push("Customer 3");
    this.customerList.push("Customer 4");
    this.stockList.push({ id: "STK1200002", desc: "ZIC SU[ER A" });
  }



  saveEntry(): void {
    var newCustomer = this.selectedCustomer;
    var newStockId = this.selectedStock.id;
    var newStockDescription = this.selectedStock.desc;
    var newQuantity = this.selectedQuantity;
    var newQuantifier = this.selectedQuantifier;

    this.entryList.push({ id: ++this.entryCount, stockId: newStockId, stockName: newStockDescription, takenQuantity: newQuantity, quantifierName: newQuantifier });
    this.data.updateList(this.entryList);
  }

}
