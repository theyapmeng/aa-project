import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgresiveDeliveryComponent } from './progresive-delivery.component';

describe('ProgresiveDeliveryComponent', () => {
  let component: ProgresiveDeliveryComponent;
  let fixture: ComponentFixture<ProgresiveDeliveryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgresiveDeliveryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgresiveDeliveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
