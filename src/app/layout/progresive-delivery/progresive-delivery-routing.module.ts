import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProgresiveDeliveryComponent } from './progresive-delivery.component';

const routes: Routes = [
  {
    path: '',
    component: ProgresiveDeliveryComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgresiveDeliveryRoutingModule { }
