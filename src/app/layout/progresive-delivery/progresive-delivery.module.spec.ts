import { ProgresiveDeliveryModule } from './progresive-delivery.module';

describe('BlankPageModule', () => {
    let blankPageModule: ProgresiveDeliveryModule;

    beforeEach(() => {
        blankPageModule = new ProgresiveDeliveryModule();
    });

    it('should create an instance', () => {
        expect(blankPageModule).toBeTruthy();
    });
});
