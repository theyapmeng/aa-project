import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeliveryMonitoringComponent } from './delivery-monitoring.component';

const routes: Routes = [
  {
    path: '',
    component: DeliveryMonitoringComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeliveryMonitoringRoutingModule { }
