import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeliveryMonitoringRoutingModule } from './delivery-monitoring-routing.module';
import { DeliveryMonitoringComponent } from './delivery-monitoring.component';
@NgModule({
  imports: [
    CommonModule,
    DeliveryMonitoringRoutingModule
  ],
  declarations: [DeliveryMonitoringComponent]
})
export class DeliveryMonitoringModule { }
