import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryMonitoringComponent } from './delivery-monitoring.component';

describe('DeliveryMonitoringComponent', () => {
  let component: DeliveryMonitoringComponent;
  let fixture: ComponentFixture<DeliveryMonitoringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryMonitoringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryMonitoringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
