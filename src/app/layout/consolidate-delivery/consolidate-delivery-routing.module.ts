import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsolidateDeliveryComponent } from './consolidate-delivery.component';

const routes: Routes = [
  {
    path: '',
    component: ConsolidateDeliveryComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsolidateDeliveryRoutingModule { }
