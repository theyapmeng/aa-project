import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsolidateDeliveryComponent } from './consolidate-delivery.component';

describe('ConsolidateDeliveryComponent', () => {
  let component: ConsolidateDeliveryComponent;
  let fixture: ComponentFixture<ConsolidateDeliveryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsolidateDeliveryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsolidateDeliveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
