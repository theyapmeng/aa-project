import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConsolidateDeliveryRoutingModule } from './consolidate-delivery-routing.module';
import { ConsolidateDeliveryComponent } from './consolidate-delivery.component';

@NgModule({
  imports: [
    CommonModule,
    ConsolidateDeliveryRoutingModule
  ],
  declarations: [ConsolidateDeliveryComponent]
})
export class ConsolidateDeliveryModule { }
