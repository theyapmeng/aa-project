import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConsignmentNoteRoutingModule } from './consignment-note-routing.module';
import { ConsignmentNoteComponent } from './consignment-note.component';
@NgModule({
  imports: [
    CommonModule,
    ConsignmentNoteRoutingModule
  ],
  declarations: [ConsignmentNoteComponent]
})
export class ConsignmentNoteModule { }
