import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsignmentNoteComponent } from './consignment-note.component';

const routes: Routes = [
  {
    path: '',
    component: ConsignmentNoteComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsignmentNoteRoutingModule { }
