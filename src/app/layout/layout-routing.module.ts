import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard' },
            { path: 'progresive-delivery', loadChildren: './progresive-delivery/progresive-delivery.module#ProgresiveDeliveryModule' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            { path: 'forms', loadChildren: './form/form.module#FormModule' },
            { path: 'bs-element', loadChildren: './bs-element/bs-element.module#BsElementModule' },
            { path: 'grid', loadChildren: './grid/grid.module#GridModule' },
            { path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
            { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' },
            { path: 'import-delivery', loadChildren: './import-delivery/import-delivery.module#ImportDeliveryModule' },
            { path: 'consignment-note', loadChildren: './consignment-note/consignment-note.module#ConsignmentNoteModule' },
            { path: 'delivery-monitoring', loadChildren: './delivery-monitoring/delivery-monitoring.module#DeliveryMonitoringModule' },
            { path: 'consolidate-delivery', loadChildren: './consolidate-delivery/consolidate-delivery.module#ConsolidateDeliveryModule' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
