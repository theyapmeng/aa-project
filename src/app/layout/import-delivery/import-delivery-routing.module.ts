import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ImportDeliveryComponent } from './import-delivery.component';

const routes: Routes = [
  {
    path: '',
    component: ImportDeliveryComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ImportDeliveryRoutingModule { }
