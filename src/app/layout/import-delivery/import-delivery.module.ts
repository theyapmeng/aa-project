import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImportDeliveryRoutingModule } from './import-delivery-routing.module';
import { ImportDeliveryComponent } from './import-delivery.component';

@NgModule({
  imports: [
    CommonModule,
    ImportDeliveryRoutingModule
  ],
  declarations: [ImportDeliveryComponent]
})
export class ImportDeliveryModule { }
