import { ImportDeliveryModule } from './import-delivery.module';

describe('BlankPageModule', () => {
    let blankPageModule: ImportDeliveryModule;

    beforeEach(() => {
        blankPageModule = new ImportDeliveryModule();
    });

    it('should create an instance', () => {
        expect(blankPageModule).toBeTruthy();
    });
});
